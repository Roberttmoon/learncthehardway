#include <stdio.h>

int main(int argc, char *argv[])
{
  int bugs = 100;
  double bugRate = 1.2;

  printf("You have %d bugs at the imaginary rate of %f.\n", bugs, bugRate);

  unsigned long long universeOfDefects = 1L * 1024L * 1020L * 1024L;
  printf("the entire universe has %llu bugs.\n", universeOfDefects);

  double expectedBugs = bugs*bugRate;
  printf("You are expected to have %f bugs.\n", expectedBugs);

  double partOfUniverse = expectedBugs / universeOfDefects;
  printf("That is only a %e portion of the universe.\n", partOfUniverse);

  // this makes no sense, just a demo of somthing weird!!!
  char nulByte = '\0';
  int carePercentage = bugs * nulByte;
  printf("Which means you should care %d%%.\n", carePercentage);

  return 0;
}
