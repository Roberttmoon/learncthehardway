#include <stdio.h>

int main(int argc, char*argv[])
{
  int distance = 100;
  float power = 2.345f;
  double super_power = 5678.4532;
  char initial = 'T';
  char first_name[] = "Robert";
  char last_name[] = "Moon";

  printf("You are %d miles away.\n", distance);
  printf("You have %f levels of power.\n", power);
  printf("You have %f awesome super powers.\n", super_power);
  printf("I have an initial %c.\n", initial);
  printf("%s is my first name and ", first_name);
  printf("my last name is %s.\n", last_name);
  printf("");
  printf("My name is %s %c %s!\n", first_name, initial, last_name);

  return 0;
}
