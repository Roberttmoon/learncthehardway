#include <stdio.h>

int main()
{
  int age = 32;
  int height = 74;

  printf("I am %d years old.\r\n", age);
  printf("I am %d inches tall.\r\n", height);

  return 0;
}
